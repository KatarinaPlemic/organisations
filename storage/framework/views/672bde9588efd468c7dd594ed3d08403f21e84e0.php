


<?php $__env->startSection('content'); ?>

<div class="wrapPage">

	<h3>Update page:</h3>
	
	<?php  foreach($selectedCompany as $company): ?>
		
		<form action="http://localhost/test1/public/updateFunc"   method="GET">
		
			<input type='hidden' value="<?php echo $company->id; ?>" name="id_company">
			
			<table id="companyInfoTable">
				<tr>
					<td>Company_name: </td>
					<td><input type="text" name="company_name" value= <?php echo $company->company_name;?> /></td>
				</tr>
				<tr>
					<td>Pib:</td>
					<td><input type="text" name="pib" value= <?php echo $company->pib;?> /></td>
				</tr>
				<tr>
					<td>Email:</td>
					<td><input type="text" name="email" value= <?php echo $company->email;?> /></td>
				</tr>
				<tr>
					<td>Description: </td>
					<td>
						<textarea name="description">
							<?php echo $company->description;?> 
						</textarea>
					</td>
				</tr>
			</table>
			
			<button type="submit" id="btnChangeAndUpdate">change</button>
		</form>
		
		
		
	<?php endforeach;?>

</div>
<?php echo $__env->make('layouts.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\test1\resources\views/companies/update.blade.php ENDPATH**/ ?>